# Violet-RS
Violet Raystorm is a Gradius-Inspired game by The Great Metaphor, Facade, and Delta. But mostly Meta.
Violet Raystorm was made in 2023 and is protected under Creative Commons 4.0 (See LICENCE file)
Tic80 was made by nesbox. See the [Tic80](https://tic80.com/learn) website for more details:
Violet Raystorm is open-source, and can be found here: <https://codeberg.org/TGMGames/Violet-RS> As of writing, Violet Raystorm's code was made in the basic version of the Tic80, not the pro version. This means I can't save the cart as a .lua file instead of a .tic file, which means that git thinks it's a binary file.

## Story
It's the year 20XX. Planet Raven is under attack from the Buredian Empire. You, the pilot of a the M-366, must go and destroy The King of Shadows, a biomechanical supercomputer that is leading the Buredian Army.
Yeah that's all you get for the plot. It's a Gradius Clone programmed on the TIC-80. Did you expect a long, drawn-out plot?

## How to run the game (for people not familiar with the Tic80)
If you're playing one of the pre-bundled versions (Windows, Linux, Mac, or browser) it should start automatically after launching the exacutable. If it doesn't, press `control+r` or run the command `run` in the console and it'll start.
If you're running it from a non-bundled version, run the command `folder` in the Tic80 Console. This will open the tic80's working directory. Move the .tic file to there. Then either:
* run the commands `open Violet-RS` and then `run`
* run the command `surf` and navigate to the folder you put it in.
I don't know how to run the game on android or other system, sorry. I'll try to have that figured out by the time I realese this game publically.

If you are familiar with the tic80 then you already know what to do.

## How to play

### Controls
Violet Raystorm can be played with with either a controler or gamepad (although it has not been tested it with gamepad):
* Arrow Keys: move
* z: shoot
* x: Redeam currently selected powerup

### Powerups and scoring.
Similar to Gradius if you've played it, collecting a powerup increments the powerup gauge in the bottom left, and pressing the powerup button redeams the currently selected powerup. The powerups are:
* B. Bomb   (Downwards attack)
* S. Spread (Slower than the default gun but also shoots upwards. replaces laser)
* L. Laser  (Peirces through most enemies, and does extra damage to enemies it can't pierce. replaces spread)
* O. Option (Dramatically increases firepower. Can be selected multiple times.)
* S. Shield (tanks 1 hit and then dissapears. Despite the sprite, it covers your entire ship, like in some of the early Gradius Games. Can be selected multiple times, if it's lost.)
Unlike Gradius, powerups are dropped when you kill 3 enemies of the same type in a row. Your progress towards your next powerup can be seen in in the top left corner of the screen, besides your score. When a powerup drops, it also gives a score bonus. Carefully choosing where to move, when to shoot, and what powerups to use is vital for getting to full power and getting an optimal score.

There's no lives in Violet Raystorm, you just lose score and all your powerups if you die. You lose more score than you can get in a single checkpoint (I think), but for a casual playthrough just focus on beating the game first.

## Progression
Every time you enter a new level, you gain the option to skip straight to that level next time you play. Levels 1-5 are part of the first loop, levels 6-11 are part of the second.
If, for whatever reason, you want to reset your progress, use the tic-80 command `folder` to open the working directory, open `.local/`, and delete the save file. Tic80 save files don't have names you'd expect, but instead hash digests of either the code or the save-id. Unless other platforms have different hashing algorythms, the file should be called `307d3473e797fe713c5f53ff3b457c9a`
