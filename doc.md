# FUNCTIONS
## functionName(name , [optional = default]) -> name
usage:
-instructions
arguments:
-<type> name: description
-[<type> optional]: description
or -none
output:
-<type> name : description
or -none

## min(n) -> x
usage:
-shortcut to math.min
## max(n) -> x
usage:
-shortcut to math.max
## abs(n) -> x
usage:
-shortcut to math.abs
## flr(n) -> x
usage:
-shortcut to math.floor
## ceil(n) -> x
usage:
-shortcut to math.ceil
## rand(n,[m]) -> x
usage:
-shortcut to math.random
## sin(n) -> x
usage:
-shortcut to math.sin
## cos(n) -> x
usage:
-shortcut to math.cos

## clamp(n,mu,ml) -> n
usage:
-if n is in between mu and ml, returns n. if n is above mu, returns mu. if n is below ml, returns ml.
arguments:
-<number> n: the input number
-<number> mu: stands for... *something* upper. The maximum allowed value.
-<number> ml: stands for *something* lower. Yeah I don't actually know what the m stands for
output:
-<number> n : the output number.

## round (n) -> n
usage:
-rounds the inputted number to the nearest whole number. shortcut to flr(n+.5)
arguments:
-<number> n : the number that gets rounded
output:
-<number> n : the number.

## spnPlr( [d = false] )
usage:
-used to spawn the player at the beggining of the game, when trasitioning levels, or after the player dies.
arguments:
-[<bool> d]: if true, it sets every one of the player's upgrades to 0.
output:
-none.

## col(x1,y1,x2,y2,s1,s2) -> collition
usage:
-used to detect collition between entities (player, projectiles, enemies, etc...)
arguments:
-<number> x1: the x position of the first point
-<number> y1: the y position
-<number> x2: the x position of the second point
-<number> y2: the y position
-[<number> s1]: how close they can be on the x axes. defaults to 8
-[<number> y1]: dhow close they can be on the y aes. defaults to 8
output:
-<bool>: returns true if they are too close together

## pcol(x,y,w,h) > collition
usage:
-used to check if there was a collition with terrain. No, I don't know where the p in its name comes from.
arguments:
-<number> x: x position of the entity. Coordinance are centered on the top left for the same of collition
-<number> y: y position of the entity
-<number> w: the width of the entity, in tiles... we think.
-<number> h: the height of the entity
output:
-<bool>: returns true if the entity colided with terrain

## hidm(t) -> t
usage:
-used by calls of the map function to hide entitiy spawning tiles.
arguments:
-<number> t: the number of the tile that is being scanned
output:
-<number> t : the number of the tile to display. It returns 0 if input t is a spawn tile, and it returns t in all other circimstances. See isSpawn or tile documentation for spawn tile ids

## isSpawn(t) - true/false
usage:
-isSpawn is used to see if a tile is a spawn tile. It's used by hidm and the spawning script.
arguments:
-<number> t: the tile being scanned
output:
-<bool> : returns true if it's a spawn tile, or false if not.
-<number> : the id of the spawn tile, which is used by the spawning script to pick which id from the table enl to spawn

## spnE(x,y,a,[...]) -> e(ntity)
usage:
-spnE is used to spawn entities. The engine uses it to spawn enemies, enemies use it to spawn powerups, projectiles, and death explotions.
arguments:
-<number> x: the x position to spawn the entity
-<number> y: the y position
-<number> a: Artificial Intelegence. the ai used by the spawned entity, as an id of the ai list.
-[<varies> ...]: any number of additional variables, that are usually numbers. These can be used to change the entity's spawning stats, like movement for a projectile.
output:
-<table> e : the entity that was just spawned. The table is formatted as such:
-1 <function> the ai of the entity
-2 <number> the x position
-3 <number> the y position
-4 <number> the state of the entity
-5 <number> this and any after this are used by the entity's ai and are ai specific.

## ai[number](s) -> bool
usage:
-every ai is used by an entity. See the ai list for which ai is associated with which entity, and how they use their additional variables
arguments:
-<table> s: the table that makes up the self
output:
-<bool> : if true, the game despawns that entity

## dBox(s,w,[h = w])
usage:
-entities can use this to give themselves a damaging hitbox. It's a very simple hitbox, centered around the entity's coordinance.
arguments:
-<table> s: self
-<number> w: the width of the hitbox
-[<number> h]: the hieght of the hitbox. defaults to w.

## pShoot(x,y,o)
usage:
-pShoot is only used three times, and that's for shooting the player's projectles. the three calls are for the player, the first option, and the second.
arguments:
-<number> x: the x position to shoot from
-<number> y: the y position
-<number> o: which option it belongs to.
output:
-none

## vecS(x,y,l) -> x,y
usage:
-vecS is used to shorten a vector until it's the same length as l. This is primarely used to quicly aim something at the player, such as in the rotating enemy's attack: `spnE(s[2],s[3],2,vecS(px-s[2],py-s[3],1))` which spawns a diamond project that moves roughly the length of one pixel towards where the player was when it was spawned every frame. WARNING! These are floats instead of integers, so be very careful with what you do with them, since many entities are not built to handle floats!
arguments:
-<number> x: the starting x position of the vector
-<number> y: the starting y position
-<number> l: the maximum lenth of the vector
output:
-<number> x: the new x length
-<number> y: the new y length

## sKil(s,w,h,[scr = 3 , n = false]) -> <bool>
usage:
-sKil, short for Small Kill, placed inside of the ais of small enemies, and handles the score and projectile collition of small enemies, but not despawning. It's also used in bKil for hit detection.
arguments:
-<table> s: self
-<number> w: width, the size of the hitbox when coliding with projectiles
-<number> h: hieght, the hieght of the hitbox
-[<number> scr]: the score droppped by the enemy when it is killed. Defaults to 3
-[<boolean> n]: if true, it won't play a sound effect. only used in bKil. Defaults to false.
output:
-<bool> : returns true if the enemy hit something. This can be used to tell the ai to change to its dieing state, or fed directly into its return on whether or not to die, for example in the rotating enemy's ai: `return (s[2] < -8) or sKil(s,10,10,5)` where it returns true if its x postion is less than 8 or of it was killed.
-[<table> b]: if it was hit, it returns what projectile hit it. This is used by bKil to detirmin how much damage was don.

## bKil(s,w,h,[scr = 3 , b = false) -> <bool>
usage:
-bKil, short for big kill, is place inside of big enemies and bosses. it handles the health and death of big enemies. It allocates internal variables 5 and 6 for health and, if applicable, i frames.
arguments:
-<table> s: self
-<number> w: width, the size of the hitbox when coliding with projectiles
-<number> h: hieght, the hieght of the hitbox
-[<number> scr]: the score droppped by the enemy when it is killed. Defaults to 3
-[<boolean> b]: true for bosses. activates i frames.
output:
-<bool> : returns true of health is less than 1

## vict([f = false])
usage:
-vict (victory) is used by the player code to determin when to either complete the game or move to the next level
arguments:
-[<bool> f]: final. if true, it plays the ending cutscene instead of going to the next, which is currently a crash screen saying that the end screen doesn't exist yet.
or -none
output:
-none

## bExpl(s,w,h) -> <bool>
usage:
-bExpl (Boss explotion) is used for the death animation in bosses
arguments:
-<table> s: self
-<number> w: the width of the explotion range
-<number> h: the hieght
output:
-<bool> : returns true if the animation is over

## kPlr()
usage:
-kPlr is used in dBox and the player's terrain collition script to kill the player. It sets the player's iframes to the correct ammount and spawns an explotion in the variable dp for the ending animation
arguments:
-none
output:
-none

## drpP(s,x)
usage:
-drpP is used in the death script of entities that drop powerups (which is basically all of them that aren't bosses), and it keeps track of when to drop powerups, and drops them.
arguments:
-<table> s: self
-<number> x: the id it has for the combo. This should always be the same id as it's ai, unless an entity either shares an ai with two different enemies or if one enemy has multiple ais
output:
-none

## pMove(s,x,y)
usage:
-is used in bullets with precice movement, and uses varaibles 7 and 8 to store the bullet's internal movement, and 2 and 3 its true position.
arguments:
-<table> s: self
-<number> x: the x amount to move by. Should be a decimle number
-<number> y: the x amount to move by. Should be a decimle number
output:
-none

## hScl(b,a) ->- number
usage:
-hScl (Health Scale) is used by bosses and a few tanky enemies to scale their health in relation to the adaptive diffaculty. Calculated as b+(a * adaptive diffaculty)
arguments:
-<number> b: the enemy's base health.
-<number> a: the amount amount that it goes up by level of adaptive diffaculty. Should be a very low number, since the best powerups contribute a deceptivly large amount to adaptive diffaculty
output:
-<number> : the enemy's new health

## rBul(s,[m=20,sp=1])
usage:
-rBul is used in the death script of a few enemies, mostly smaller ones, to shoot revenge bullets.
arguments:
-<table> s: self
-[<number> m]: the adaptive diffaclty level before it starts shooting.
-[<number> sp]: the speed of the slowest bullet
output:
-none

## rBar(s)
usage:
-used by boss enemies (and the white blood cell) to tell the game how much total health all the active bosses have.
arguments:
-<table> s: self
output:
-none

# GLOBAL VARIABLES:
<number> frm: frame. Which frame is on. Used for RNG calls and timers.
<string> state: controles the state of the game. The different recongized states:
* menu: 				main menu
* sgame:				Starting the game from the main menu
* game:					game
* death: 				dieing animation and game restarting
<number> sLevel: Selected Level, the level selectec in the main menu
<number> pLevel: playing Level the level that's being played
<number> scrl: SCRoll Level, the amount that the level has scrollled in tiles
<number> sTim: scroll timer, the amount of time before it stroles another tile
<number> sSpd: scroll speed, the speed at which the level scrolls. Lower numbers equals faster scroll
<table> solid: what tiles will be considered solid
<table> enl: Enemy List, What enemies will spawn in that level.
<table> aae: All Active Entities, list of active entities
<number> ce1: checkpoint 1, the the x position of the first checkpoint. This is in relation to the left side of the screen in tiles.
<number> ce2: checkpoint 2. The second checkpoint in the level, if there are two.
<number> sl: spawn slocation, te place that scrl will start at when the player spawns.
<number> sMus: the selected music outside of bosses
<number> tCel: what peice of terrain to select
<number> ad: Adaptive diffaculty.
<table> message: the message displayed at the end of levels 5, 10, and 11. Every line is a string.
<number> bhBar: How much leath the boss is displayed to have

## variables related to the player
<number> score: you get it when you kill stuff or get combos, and you lose it when you die.
<number> hScore: high score. (stored in ram)
<number> lScore: how much score to lose when you die.
<number> prog: progress. Stored in ram.
<number> px,py: the x and y coordinance of the player.
<number> pif: the player's iframes.
<table> pu: the player's upgrades. Contents of the table, in the order they are expected to appear:
* <number> 1: the player's missles 0 is no missles. 1 is missles for going down. 2 is reserved for a 2 way missle.
* <number> 2: the player's primary weapon. 0 is straight shot. 1 is spread. 2 is reserved for a 3-way spread shot in case it is added. 3 is laser.
* <number> 3: the amount of options the player has.
* <number> 4: the player's sheild. 0 is no sheild. 1-? is more sheild
<table> pj: the players projectiles.
* <table> 1,2,3...: projectiles. Projectile tables follow this format: {x,y,t,o} t is what type the projectile is. o is which option it belongs to, starting with 1 (the player)
* <table> b: the number of bullets active per option
* <table> m: the number of missles active per option
* <number> [projectile types]: While not an internal variable, these are the types of player projectiles: 0 is a foward-facing bullet. 1 is a an upward bullet. 2 is reserved for downwards bullets. 3 is unused (accidently reserved two bullet types for upwards bullet), 4 is laser. 5 is reserved for upwards missle. 6 for downward missle
* <table> pd: player death particle, used by the death animation to animate the player's death by spawning an explotion thingy to it
* <number> ps: Powerup Selector,which powerup is selected
* <number> pc: powerup combo how many enemies have been killed since the last powerup was dropped
* <number> pl: powerup last, which enemy was used in a combo last
* <table> po: player options, the locations of the option trail. only 8 and 16 are visible, and only if the appropriate number of powerups are gotten
* <number> pt: player timer. The next frame the player is allowed to shoot on.

# ai list:
Unless stated otherwise, any timer variable is measured in relation to the variable frm, rather than an entity's internal timer.
0. Warning Signal
States: 0 is spawn, 1 is display. Yes, something as simple as this requires displays
** 5: The message display. This is a string, not a number.
** 6: The x position to draw at. This is set internally by state 0 and does not need to set by whatever spawns it.
** 7: The frame is spawned, used to determin when it should despawn. also set by state 0
1. Rolem (rotating enemy)
states: 0 is spawn. 1 is moving foward and waiting to move vertically. 2 is moving vertically. 3 is moving foward until despawn.
** 5: the y position it moves to after a few frames. This is its starting y position, flipped across the middle of the screen.
** 6: how long to wait before moving vertically, measured in which frame to do so.
** 7: which direction to move when moving vertically
** 8: how long to wait before shooting. measured in which frame to do so.
2. diamond-shaped bullet
this entity does not make use of states
** 5: the x speed of the bullet
** 6: the y speed
** 7: the internal x position of the bullet. Every tick, the game redefines the true x and y position to be this number, rounded down. This gives
** 8: the internal y position
3. laser-shaped bullet
states: 0 does a sound effect. This is its only use of states.
** 5: x speed. lasers have no y speed.
4. missle
this entity does not make use of states.
** 7: The internal x psoition of the bullet
** 8: the internal y position.
5. Third Blade
States: 0 is spawn, 1 is moving into position, 2 is the main fight, 3 is the death animation
** 5: health
** 6: iframes
** 7: attack order. This is a table, not a number.
** 8: Whether it's the normal version or level 11 version.
6. Explotion
this entity does not make use of states
** 5: the size of the explotion. the smaller the number, the larger the explotion
** 6: the animation progress of the explotion
7. Eletrict Node
States: 0 is spawn, 1 is normal functionality.
** 5: is health. Does not take damage or shoot of it's own ovolution of health is greater than 50 (ei spawned by the LG boss)
** 6: Orientation. Controles shoot direction and image direction.
** 7: used by LG's minions to force them to shoot.
** 8: fractional x movement
8. Elletric Orb
States: 0 is 'normal' movement, 1 is erratic movement.
** 5: direction. Uses the same direction logic as spr's rotation
** 6: lifetime.
** 7: fractional x movement.
9. Lightening Generator
States: 0 is spawn, 1 is movement.
** 5: health
** 6: iframes
** 7: the list of spawned minions. Fun fact! LB only cares about the top row of nodes, and ignores the bottom row.
** 8: which node it attacked with last. It's a bit funky, but that just creates some interesting patterns.
** 9: precice movement.
** 10: Timer for coming on screen
10. KoS
States: 0 is spawn, 1 is intro cutscene, 2 is phase 1, 3 is first explotion, 4 is phase 2, 5 is ending expltoion
** 5 is health
** 6 is iframes.
11. Kos Bullet
same ias entity 22 (diamond bullet)
12. powerup
This entiy does not make use of states and has no additional variables
13. Splicer/Laser enemy/horizontal simatry enemy
State 0 is startup, state 1 is it's main mode
** 5 is its starting frame
** 6 is whether or not its allowed to attack
14. Doro Doro v1
state 0 is setup. State 1 is horozontal movement. State 2 is attack.
** 5 is health
** 6 is iframes, only used for the boss varients
** 7 is the sub version. (1 attacks once with lightening, 2 attacks multiple times with bullets bullets, 3 and 4 are the boss varients.
** 8 is vertical movement direction. Not used by varient 1
15. Doro Doro v2
this entity doesn't function like most entities. All it does is set it's seventh internal variable to 2 and then turn itself into ai14.
16. Doro Dor boss handler
States: 0 is setup, 1 is waiting for the Doro Doro's to despawn, 2 is end.
This entity doesn't tunction like most entities. It just spawns two Doro Doros, changes their subversion to 3 and 4, and then waits until it's the last entity left alive
17. Red Blood Cell
States: 0 is startup, 1 is everything else.
** 5 is health
** 6 is its current direction. Follows spr direction rules
** 7 is whether or not its turning
18. antibody
States: 0 is startup, 1 is everything else
** 5 is whether or not it has captured an option.
19. White Blood Cell
States: 0 is startup, 1 is everything else
** 5: health
** 6: splits left (only used for the level 10 varient)
20. Gooser
States: 0 is startup, 1 is movement, 2 is shooting
** 5: is whether the gooser is on the ceiling or not. -1 is ceiling, 1 is upright
** 6: horizontal orientation. -1 is facing left, 1 is facing right.
** 7: is the amount of ammo it has left.
** 8: is the time until it takes its next shot.
21. Turrote
This entity uses the state variable, s[4], as its shooting timer.
22. Crokonde
States: 0 is startup, 1 is everything else.
** 5 is health
** 6 is vertical orientation
** 7 is its first shot. Both of these are tables.
** 8 is it's second
23. Kill Pod
States: 0 is startup, 1 is everything else
** 5 is gravity direction
** 6 is cooldown
** 7 is precise x position
** 8 is precice y position
** 9 is y speed
24. Slime Worm
States: 0 is startup, 1 is everything else
** 5 is health.
25. Heavy Rollen
States: 0 is startup, 1 is main phase, 2 is death.
** 5 is health
** 6 is iframes
** 7 is the timer it uses to fly on screen gradually.
26. Turner
states: 0 is spawn. 1 is moving foward and waiting to move vertically. 2 is moving vertically. 3 is moving foward until despawn.
** 5: the y position it moves to after a few frames. This is its starting y position, flipped across the middle of the screen.
** 6: how long to wait before moving vertically, measured in which frame to do so.
** 7: which direction to move when moving vertically
** 8: how long to wait before shooting. measured in which frame to do so.
Yes I just lazily copied and pasted it's ai from the rollen. Sue me, there's plenty of codespace left.
27. Cutter
States: 0 is spawn, 1 is everything else.
** 5: What frame of it's one second cycle to shoot on.
28. Doron
States: 0 is spawn, 1 is intro, 2 and 3 are its main attacks, and 4 is death.
** 5: is health
** 6: iframes
** 7: vertical direction
29. Tankatron
States: 0 is spawn, 1 is main attack, 2 is death
** 5: Health
** 6: Iframes.
30. Holardian
States: 0 is spawn, 1 is transition phase, 2 is phase 1, 3 is phase 2, 4 is phase 3, 5 is death animation of phases 1 and 2, 6 is the final death animation
** 5: health
** 6: iframes
** 7: spawned enemy type 1
** 8: Spawned enemy type 2
** 9: attack timer
** 10: whether to show the core
** 11: table for the croconedes in phase 1
31. Bulwark
States: 0 is spawn, 1 is intro animation, 2 is main attack, 3 is death
** 5: Health
** 6: iframes
** 7: this one is used for the timer for the intro animation and the vertical momentum in the main attack


# Enemy selections per level:
Level 1:
* 1,1,20,13,21,0,0,5; Rollen, Rollen (again), Gooser, Splicer, Turrote, nothing, nothing, and Third Blade
Level 2:
* 1,13,21,22,14,15,0,16: Rollen, Spicer, Turrote, Crokonde, Doro Doro (elletric), Doro Doro (spread), nothing, and Doro Doro (boss)
Level 3:
* 1,13,13,23,7,0,0,9: Rollen, Spicer, Splicer (Again), Kill Pod, Elletric Node, nothing, nothing, and Lightening Genorator
Level 4:
* 1,20,0,22,24,0,0,25: Rollen, Gooser, nothing, Crokende, Slime Worm, nothing, nothing, and Heavy Rollen
Level 5:
* 1,20,23,17,18,19,0,10: Rollen, Gooser, Kill Pod, Red Blood Cell, Antibody, White Blood Cell, Noting, and King of Shadows

Level 6:
* 1,26,20,13,13,27,21,31: Rollen, Turner, Gooster, Splicer, Splicer (again), Cutter, Turrote, Bulwerk
Level 7:
* 1,27,1,22,14,15,24,28: Rollen, Cutter, Rollen (again), Crokonde, Doro Dor (elletric), Doro Doro (spread), Slime Worm, and Doron
Level 8:
* 27,0,0,0,0,0,0,30: Cutter, nothing x6, and Holardian
Level 9:
* 26,20,1,22,24,27,23,29: Turner, Gooser, Rollen, Crokonde, Slime Worm, Cutter, Kill Pod, Tankatron
Level 10:
* 26,20,23,17,0,19,7,10: Turner, Gooser, Kill Pod, Red Blood Cell, nothing, White Blood Cell, Elletric Node, and King of Shadows

Level 11:
* 1,13,26,27,21,4,18,5: Rollen, Splicer, Turner, Cutter, Turrote, Third Blade bomb, and Third Blade.


# Persistent Memory
* 0x00: high score
* 0x01: progress

# Sprites:
all sprites are expected to be in 2 bit mode
formatted as [name of sprite][tiles]
single number: one tile makes up the sprite
number-number: all tiles on that line make up a single sprite or animation
number, number x number: a larger sprite. First number is the upper left corner, then the dimentions

## forground tiles (player, enemies, stuff like that)
* Player: 				            512-517
** Option: 				            544
** Player's bullet: 			    545-546
** Player's lase: 			        547
** Player's bomb: 			        548
** Sheild: 				            549
** Powerup: 				        518
* Rotational Enemy:                 519-520
* small enemy bullet:               521
* laser enemy bullet:               522
* missle enemy bullet:              523
* Turner:                           524,    4x2
* Red Blood Cell:                   528     6x2
* Upgade bar:                       550,    6x1
* Laser Enemy:                      576,    2x2
* Bomb enemy:                       578,
* Crokonde:                         580,    2x2
* Antibody:                         584,    4x1
* Cutter:                           588,    4x2
* White Blood Cell:                 592,    8x3
* Combo Meter:                      582-583
* Slime Worm:
** Head:                            610
** Body:                            579,    1x2
* Gooser:                           614,    6x2
* Elletric Orb:                     640,    6x2
* Kill Pod:                         678,    6x2



## background tiles
* Blank slot: 				        0
* Level 1 (Planet under attack):	0,      6x4 (EXCLUDING TOP LEFT CORNER, THAT'S THE BLANK SLOT)
** collition (on spritesheet):      34,     2x2
** collition (mget adress):         18,19,66,67
* spawner sprites:                  6-14
* Star Graphics/Level 2:            128,    6x4
** no collition tiles
* Goop/Level 3                      38,     6x3
** collition (on spritesheet):      all but 105 and 106
** collition (mget adress):         22,23,24,25,26,38,39,40,41,42,54,55,56
* Grass and Pipes/Level 4           134,    4x1;    166,    2x2
** Having no unique solid tiles of its own, stage 4 uses the top 2 tiles in stage 1 and the blocks of stage 5 as its collition tiles.
* LOGO: 				            256,    8x8
* Mechanical/Level 5:               264,    8x3
** collition (on spritesheet):      264,265,266,271,296,298,303,328,329,330,334
** collition (mget adress):         136,137,138,152,154,168,169,170,171,143,159
* Holoardian's tileset:             360,    8x2
### second page, bosses and stuff:
* Third Blade:                      16,     8x8
* Lightening node:                  24,     3x3
* Doro Doro:                        28,     4x2
** Doro Doro cap varients:          27,     1x8
* Lightening Gen:                   92,     4x5
* Heavy Rollen:                     120,    4x8
* Doron:                            272,    8x3
* Kos:                              280,    8x8
* Tankatron:                        368,    8x3
** Tankatron Wheales:               464,    4x2
* Bulwerk:                          468,    4x2

## Other sprites:
* Holardian:
Holardian doesn't have a sprite to itself. Instead, it uses part of the map as its sprites for it's first and second phases, and the core just reuses the lightening orb's sprite

# SFX, sound channels, and instraments:
sound channel 0 is reserved for all sfx. Sound channels 1-3 are all for music. These channels are labled 1-4 on the sound channel

## 0-15 reserved for sfx
0. warning
1. enemy laser shot
2. Antibody spawn sound
4. player explotion
5. elletric orb
6. Motrar shot
8. player shot
9. powerup collect
12. small explotion
13. powerup selection

## 16-31 not yet defined

## 32-47 not yet defined

## 48-63 reserved for music instraments
48. Hard Lead
49. Hard Synth
50. tryinagle synth
51. not sure what was doing with this?
52. Soft Synth
61. That weird wave sound used in track 4
62. Soft Melodic percussion
63. drum


# Music patterns
unused patterns:
* 59, 60
0: Main menu:    c#major, done (?)
* excusivly barrows patterns from track 1
1: stage 1 theme:c#major , done
* 1-4,10-14, 20 and 21.
2: stage 2 theme:B major?, done
* 9, 19, 23-26, 27 , 38, 58
3: stage 3 theme:B major , done
* 5-8,15-18,22
4: stage 4 theme:C#major , done
* 34-35, 49
* borrows patterm 13 from track 1
5: boss,         c #major, complete
* 30-33,40-42,50-53.
6: final boss    c#major, done
* 43-48 , 54-55  c#Major,
* borrows patterns 1,2,3, 20 and 21 from track 1
7: credits theme?
*  (excussivly?) borrows patterns from Tracks 1, 3 and 6
